"""
Transmission Analysis Script
===========================

This script performs analysis and comparison of different cipher implementations in various transmission scenarios.
"""

from src.scenarios import scenario, node
from rich.prompt import Prompt
from rich.console import Console
from rich.markdown import Markdown
import matplotlib.pyplot as plt


class Transmission:
    """
    Class for performing transmission analysis and generating plots.
    """

    def __init__(self):
        """
        Initialize the TransmissionAnalyzer class.
        """
        # Get the plaintext message to be transmitted
        message = Prompt.ask("What is the message you want to transmit ?")
        self.plaintext = message.encode('utf-8')
        self.plaintext_size = len(self.plaintext)

        ###### Set up the scenario
        choice = Prompt.ask("\nChoose between the following scenarios:\n0: default \n1: end_to_end\n2: ground_to_satellite\n3: satellite_to_satellite\n",
                            choices=['0', '1', '2', '3'], default='0')
        if choice == '0':
            self.scene = 'default'
            default = node(1, 1, 1/self.plaintext_size, self.plaintext)
            self.scenarii = scenario(default, default, 1, 1, 1, self.plaintext)
        if choice == '1':
            self.scene = 'end_to_end'
            end_user = node(16e9, 8e9, 3.5e9, self.plaintext)
            self.scenarii = scenario(end_user, end_user, 13e9, 1600e3, 3 * 10 ** 8, self.plaintext)
        elif choice == '2':
            self.scene = 'ground_to_satellite'
            satellite = node(2e9, 512e6, 100e6, self.plaintext)
            ground_station = node(1e12, 32e9, 4e9, self.plaintext)
            self.scenarii = scenario(ground_station, satellite, 13e9, 500e3, 3 * 10 ** 8, self.plaintext)
        elif choice == '3':
            self.scene = 'satellite_to_satellite'
            satellite = node(2e9, 512e6, 100e6, self.plaintext)
            self.scenarii = scenario(satellite, satellite, 281.7e12, 7371e3, 3 * 10 ** 8, self.plaintext)


    def run_analysis(self):
        """
        Run the transmission analysis and generate plots.
        """

        ###### Plot of the result
        plt.figure('All of them in a ' + self.scene + ' scenario')
        for mode in self.scenarii.costs.keys():
            for cipher in self.scenarii.costs[mode].keys():
                for combination in self.scenarii.costs[mode][cipher].keys():
                    plt.barh(combination, self.scenarii.costs[mode][cipher][combination])
        plt.ylabel("Algorithm-key_size/nonce_block_size")
        plt.xlabel("Cost")
        plt.yticks(fontsize=9)
        plt.title(f"Comparative anaysis for a {self.plaintext_size}byte long message")
        plt.tight_layout()  # Ensures labels don't overlap
        plt.show()

        # Keep only the best combinations
        self.scenarii.choose_the_better()

        plt.figure('Now only the best combination for each cipher in a ' + self.scene + ' scenario')

        for mode in self.scenarii.costs.keys():
            for cipher in self.scenarii.costs[mode].keys():
                combinations = list(self.scenarii.costs[mode][cipher].keys())
                costs = list(self.scenarii.costs[mode][cipher].values())
                
                bars = plt.barh(combinations, costs)

                # Adding text labels for each bar
                for bar, cost in zip(bars, costs):
                    label = f'{cost:.6f}' if cost > 0.000001 else f'{cost:.2e}'
                    plt.text(bar.get_width(), bar.get_y() + bar.get_height()/2, label, 
                            va='center', fontsize=8, color='black', fontweight='bold')

        plt.ylabel("Algorithm-key_size/nonce_block_size")
        plt.xlabel("Cost")
        plt.yticks(fontsize=9)
        plt.title(f"Comparative analysis for a {self.plaintext_size} byte long message")
        plt.tight_layout()  # Ensures labels don't overlap
        plt.show()

        

if __name__ == "__main__":
    # Create a rich console for better user interaction
    console = Console()

    # Display instructions from the markdown file
    with open("instruction.md") as readme:
        markdown = Markdown(readme.read())
    console.print(markdown)

    # Ask user if they want to start
    start = Prompt.ask("Do you want to start?", choices=["y","n"], default="y")
    if start == 'n':
        exit(1)
    # Create an instance of TransmissionAnalyzer and run the analysis
    transmission = Transmission()
    transmission.run_analysis()
