# satcom_cryptography

Performance Metrics:
        - result: The output or result of the cryptographic algorithm.
        - implementation_size: The size of the algorithm's implementation in bytes.
                              A smaller implementation size is generally preferred for efficiency.
        - ram_consumption: The amount of memory consumed by the algorithm during execution.
                           Lower RAM consumption is desirable, especially in resource-constrained environments.
        - throughput: The processing speed of the algorithm, measured in bytes per CPU cycle.
                      Higher throughput values indicate faster processing speeds.
        - execution_time: The total execution time of the cryptographic algorithm in CPU cycles.
                          Lower execution times indicate faster algorithm performance.
        - memory_usage: The current memory usage at the end of the algorithm's execution.
                        Provides insights into memory consumption and potential memory leaks.
        - cpu_cycles: The total number of CPU cycles consumed by the algorithm.
                      Indicates the computational effort required by the algorithm.


-----> First i Implement some well Known BLOCK Cipher
-> About AES :
    Documentation : https://github.com/Legrandin/pycryptodome

-> About DES :
    Documentation : https://github.com/Legrandin/pycryptodome

-> About Simon & Speck : 
    Documentation : https://github.com/inmcm/Simon_Speck_Ciphers/tree/master/Python/simonspeckciphers

-> SIMECK :
    Documentation : https://github.com/bozhu/Simeck/tree/master

-> PICCOLO :
    Documentation : https://github.com/adipokala/piccolo-cipher/tree/master





-----> Then, if Time, i implement some well Known STREAM Cipher

-> About AES-CTR:
    Documentation : https://github.com/Legrandin/pycryptodome

-> About ChaCha20 : 
    Documentation : https://github.com/Legrandin/pycryptodome
    ChaCha20 is commonly used in various applications, including secure messaging protocols like Signal and WhatsApp, as well as in the transport layer security (TLS) protocol.

-> Salsa20 :
    Documentation : https://github.com/Legrandin/pycryptodome

-> SNOW :

-> Trivium :

-> Grain-128 