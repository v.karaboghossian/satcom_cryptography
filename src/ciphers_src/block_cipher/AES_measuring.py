import Crypto.Cipher.AES as AES
import os

from src.ciphers_src.util import measure_software_performance, pkcs7_padding, pkcs7_unpadding


'''
        AES :
            works with 16 (128 bits), 24 (192 bits), or 32-byte (256 bits) long key and block
        
        AES encryption supports various modes of operation, including ECB (Electronic Codebook) mode.
        ECB mode is a basic mode that divides the plaintext into blocks and encrypts each block separately using the same key.
        However, ECB mode does not provide semantic security, as identical plaintext blocks will produce identical ciphertext blocks.
        It is recommended to use more secure modes like CBC (Cipher Block Chaining) or GCM (Galois/Counter Mode) for most scenarios.
'''


MODE = AES.MODE_ECB

LENGTHS = [[16,16],[24,16],[32,16]]
KEY_SIZES = [length[0] for length in LENGTHS]
BLOCK_SIZES = [length[1] for length in LENGTHS]
KEYS = [os.urandom(key_size) for key_size in KEY_SIZES]  # Generate a random N-byte long key




def encryption_measurement(plaintext, key, key_size, block_size, mode):
    """
    Measures the performance metrics of the AES encryption operation.

    Parameters:
        plaintext (bytes): The plaintext to be encrypted.
        key (bytes): The encryption key.
        mode (int): The encryption mode.

    Returns:
        list: A list containing the performance metrics for encryption:
              [result, implementation_size, ram_consumption, throughput, execution_time, memory_usage, cpu_cycles]
    """
    perf = measure_software_performance(pkcs7_padding, plaintext, block_size)
    plaintext = perf[0]
    measure = measure_software_performance(AES.new, key=key, mode=mode)
    perf[0] = measure[0]
    for i in range(1,len(measure)):
        perf[i] += measure[i]
    cipher = perf[0]
    perf[3] = 0 #we dont take in count the throughput of the padding
    metrics = [object for object in perf[1:]]

    metrics = [b''] + metrics
    perf = measure_software_performance(cipher.encrypt, plaintext)
    metrics = [metrics[i] + perf[i] for i in range(len(perf))]
    return metrics


def decryption_measurement(encryptedtext, key, key_size, block_size, mode):
    """
    Measures the performance metrics of the AES decryption operation.

    Parameters:
        encryptedtext (bytes): The encrypted text to be decrypted.
        key (bytes): The decryption key.
        mode (int): The decryption mode.

    Returns:
        list: A list containing the performance metrics for decryption:
              [result, implementation_size, ram_consumption, throughput, execution_time, memory_usage, cpu_cycles]
    """
    perf = measure_software_performance(AES.new, key=key, mode=mode)
    
    cipher = perf[0]
    metrics = [object for object in perf[1:]]
    metrics = [b''] + metrics
    perf = measure_software_performance(cipher.decrypt, encryptedtext)
    metrics = [metrics[i] + perf[i] for i in range(len(perf))]

    perf = measure_software_performance(pkcs7_unpadding, metrics[0], block_size)
    metrics[0] = perf[0]
    perf[3] = 0 #we dont take in count the throughput of the padding
    for i in range(1,len(metrics)):
        metrics[i] += perf[i]

    return metrics

