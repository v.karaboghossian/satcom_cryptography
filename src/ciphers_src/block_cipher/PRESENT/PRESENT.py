# 0   1   2   3   4   5   6   7   8   9   a   b   c   d   e   f
Sbox = [0xc, 0x5, 0x6, 0xb, 0x9, 0x0, 0xa, 0xd, 0x3, 0xe, 0xf, 0x8, 0x4, 0x7, 0x1, 0x2]
Sbox_inv = [Sbox.index(x) for x in range(16)]
PBox = [0, 16, 32, 48, 1, 17, 33, 49, 2, 18, 34, 50, 3, 19, 35, 51,
        4, 20, 36, 52, 5, 21, 37, 53, 6, 22, 38, 54, 7, 23, 39, 55,
        8, 24, 40, 56, 9, 25, 41, 57, 10, 26, 42, 58, 11, 27, 43, 59,
        12, 28, 44, 60, 13, 29, 45, 61, 14, 30, 46, 62, 15, 31, 47, 63]
PBox_inv = [PBox.index(x) for x in range(64)]


key_size = [10,16]


class Present :

    def __init__(self, key, rounds=32):
        
        self.rounds = rounds     
        # Check if the 'key' parameter is provided and valid
        if key is None:
            raise TypeError("Missing 'key' parameter")
        elif not isinstance(key, bytes):
            raise ValueError("Invalid key format. Key must be provided as a bytes")
        elif len(key) not in key_size  :
            raise ValueError("Incorrect PRESENT key length (%d bytes)" % len(key))
        else :
            self.key_size = len(key)
            self.key = int.from_bytes(key, byteorder='big')
            self.roundkeys = generateRoundKeys(self.key, (self.key_size)*8, self.rounds) 


    def encrypt(self, plaintext):
        # Verify plaintext type
        if not isinstance(plaintext, bytes):
            raise ValueError("Invalid plaintext format. Must be provided as  a bytes")

        plaintext = pkcs7_padding(plaintext, self.get_block_size())

        cipher = b''

        # On each block
        for i in range(0, len(plaintext), self.get_block_size()):
            block = plaintext[i: i + self.get_block_size()]
            block = int.from_bytes(block, byteorder='big')
            for j in range(self.rounds - 1):
                block = addRoundKey(block, self.roundkeys[j])
                block = sBoxLayer(block)
                block = pLayer(block)

            block = addRoundKey(block, self.roundkeys[-1])

            cipher += block.to_bytes(self.get_block_size(), byteorder='big')
        return cipher

    def decrypt(self, ciphertext):
        plaintext = b''

        # On each block
        for i in range(0, len(ciphertext), self.get_block_size()):
            block = ciphertext[i: i + self.get_block_size()]
            block = int.from_bytes(block, byteorder='big')

            for j in range(self.rounds - 1):
                block = addRoundKey(block, self.roundkeys[-(j + 1)])
                block = pLayer_inv(block)
                block = sBoxLayer_inv(block)

            block = addRoundKey(block, self.roundkeys[0])

            plaintext += block.to_bytes(self.get_block_size(), byteorder='big')

        plaintext = pkcs7_unpadding(plaintext, self.get_block_size())

        return plaintext

    def get_rounds(self):
        return self.rounds

    def get_block_size(self) :
        return 8
    
    def get_key_size(self) :
        return (self.key_size)



def generateRoundKeys(key, key_size, rounds):

    roundkeys = [key >> key_size-64]

    if key_size == 80 :
        shift = 61
        third_step_start = 15
    
    elif key_size == 128 :
        shift = 64
        third_step_start = 62


    for i in range(1, rounds):

        key = ((key & (2 ** 19 - 1)) << shift) + (key >> (key_size-shift))

        key = (Sbox[key >> (key_size-4)] << (key_size-4)) + \
            (key & (2 ** (key_size-4) - 1))
        
        key ^= i << third_step_start
    

        roundkeys.append(key >> key_size-64)

    return roundkeys



def pkcs7_padding(plaintext, block_size):
    if len(plaintext) % block_size == 0:
        return plaintext
    else :
        padding_size = block_size - len(plaintext) % block_size
        padding = bytes([padding_size] * padding_size)
        return plaintext + padding


def pkcs7_unpadding(plaintext, block_size):
    padding_size = plaintext[-1]

    # Check padding size
    if padding_size <= 0 or padding_size > block_size:
        return plaintext

    # Check padding bytes
    padding_bytes = plaintext[-padding_size:]
    for byte in padding_bytes:
        if byte != padding_size:
            raise ValueError("Invalid padding bytes")

    return plaintext[:-padding_size]


def addRoundKey(state, roundkey):
    return state ^ roundkey

def sBoxLayer(state):
    output = 0
    for i in range(16):
        output += Sbox[( state >> (i * 4)) & 0xF] << (i * 4)
    return output

def sBoxLayer_inv(state):
    output = 0
    for i in range(16):
        output += Sbox_inv[( state >> (i * 4)) & 0xF] << (i * 4)
    return output

def pLayer(state):
    output = 0
    for i in range(64):
        output += ((state >> i) & 0x01) << PBox[i]
    return output

def pLayer_inv(state):
    output = 0
    for i in range(64):
        output += ((state >> i) & 0x01) << PBox_inv[i]
    return output



'''if __name__ == "__main__":
    
    key = os.urandom(16)
    cipher = Present(key)

    plaintext = b"JE suis un test"*11
    print(plaintext)
    print(len(plaintext))

    ciphertext = cipher.encrypt(plaintext)
    decryptedtext = cipher.decrypt(ciphertext)

    print(decryptedtext)
    print(len(decryptedtext))

    print(decryptedtext == plaintext)'''






