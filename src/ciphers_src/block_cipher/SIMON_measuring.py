from simon import SimonCipher
import os



from src.ciphers_src.util import measure_software_performance, pkcs7_padding, pkcs7_unpadding


'''
        SIMON:
            SIMON is a block cipher that supports various key and block sizes.
            The key size options are:
                - 64 bits (8 bytes)
                - 72 bits (9 bytes) or 96 bits (12 bytes)
                - 96 bits (12 bytes) or 128 bits (16 bytes)
                - 96 bits (12 bytes) or 144 bits (18 bytes)
                - 128 bits (16 bytes), 192 bits (24 bytes), or 256 bits (32 bytes)
            The block size options are:
                - 32 bits (4 bytes)
                - 48 bits (6 bytes)
                - 64 bits (8 bytes)
                - 96 bits (12 bytes)
                - 128 bits (16 bytes)
        
        For example, SIMON-64/32 refers to a SIMON cipher with a 64-bit key and a 32-bit block size.
'''

MODE = 'ECB'
LENGTHS = [[8*8, 4*8], [12*8, 6*8], [12*8, 8*8], [16*8, 8*8], \
          [16*8, 16*8], [24*8, 16*8], [32*8, 16*8]]
KEY_SIZES = [length[0] for length in LENGTHS]
BLOCK_SIZES = [length[1] for length in LENGTHS]
KEYS = [int.from_bytes(os.urandom(key_size//8)) for key_size in KEY_SIZES]  # Generate a random N-byte long key



def encryption_measurement(plaintext, key, key_size, block_size, mode):
    """
    Measures the performance metrics of the SIMON encryption operation.

    Parameters:
        plaintext (bytes): The plaintext to be encrypted.
        key (int): The encryption key as an integer.
        key_size (int): The size of the encryption key in bits.
        block_size (int): The size of the block in bits.
        mode (str): The encryption mode (e.g., 'ECB').

    Returns:
        list: A list containing the performance metrics for encryption:
              [result, implementation_size, ram_consumption, throughput, execution_time, memory_usage, cpu_cycles]
    """
    
    perf = measure_software_performance(pkcs7_padding, plaintext, block_size)
    plaintext = perf[0]
    measure = measure_software_performance(SimonCipher, key, key_size, block_size, mode)
    perf[0] = measure[0]
    perf[3] = 0 #we dont take in count the throughput of the padding
    for i in range(1,len(perf)):
        perf[i] += measure[i]
    cipher = perf[0]
    metrics = [object for object in perf[1:]]

    metrics = [b''] + metrics
    for i in range(0, len(plaintext), block_size//8):
        chunk = int.from_bytes(plaintext[i : i + (block_size)//8], byteorder='big', signed=False)
        perf = measure_software_performance(cipher.encrypt, chunk)
        for i in range(0,len(perf)):
            if i == 0 :
                perf[i] = perf[i].to_bytes(block_size // 8, byteorder='big', signed=False)
            metrics[i] = metrics[i]+perf[i]

    return metrics


def decryption_measurement(encryptedtext, key, key_size, block_size, mode):
    """
    Measures the performance metrics of the SIMON decryption operation.

    Parameters:
        encryptedtext (bytes): The encrypted text to be decrypted.
        key (int): The decryption key as an integer.
        key_size (int): The size of the decryption key in bits.
        block_size (int): The size of the block in bits.
        mode (str): The decryption mode (e.g., 'ECB').

    Returns:
        list: A list containing the performance metrics for decryption:
              [result, implementation_size, ram_consumption, throughput, execution_time, memory_usage, cpu_cycles]
    """
    perf = measure_software_performance(SimonCipher, key, key_size, block_size, mode)
    cipher = perf[0]
    metrics = [object for object in perf[1:]]

    metrics = [b''] + metrics
    for i in range(0, len(encryptedtext), block_size//8):
        chunk = int.from_bytes(encryptedtext[i : i + (block_size)//8], byteorder='big', signed=False)
        perf = measure_software_performance(cipher.decrypt, chunk)
        for i in range(0,len(perf)):
            if i == 0 :
                perf[i] = perf[i].to_bytes(block_size // 8, byteorder='big', signed=False)
            metrics[i] = metrics[i]+perf[i]
    perf = measure_software_performance(pkcs7_unpadding,  metrics[0], block_size)
    metrics[0] = perf[0]
    perf[3] = 0 #we dont take in count the throughput of the padding
    for i in range(1,len(metrics)):
        metrics[i] += perf[i]

    return metrics
