import sys
import time
import tracemalloc

def measure_software_performance(func, *args, **kwargs):
    """
    Measures the performance metrics of a cryptographic algorithm.

    Parameters:
        func (function): The cryptographic algorithm to be measured.
        *args: Variable-length argument list for the algorithm.
        **kwargs: Arbitrary keyword arguments for the algorithm.

    Returns:
        list: A list containing the result of the algorithm and the performance metrics:
               (result, implementation_size, ram_consumption, throughput, execution_time, 
               memory_usage, cpu_cycles)

    Performance Metrics:
        - result: The output or result of the cryptographic algorithm.
        - implementation_size: The size of the algorithm's implementation in bytes.
                              A smaller implementation size is generally preferred for efficiency.
        - ram_consumption: The amount of memory consumed by the algorithm during execution.
                           Lower RAM consumption is desirable, especially in resource-constrained environments.
        - throughput: The processing speed of the algorithm, measured in bytes per CPU cycle.
                      Higher throughput values indicate faster processing speeds.
        - execution_time: The total execution time of the cryptographic algorithm in CPU cycles.
                          Lower execution times indicate faster algorithm performance.
        - memory_usage: The current memory usage at the end of the algorithm's execution.
                        Provides insights into memory consumption and potential memory leaks.
        - cpu_cycles: The total number of CPU cycles consumed by the algorithm.
                      Indicates the computational effort required by the algorithm.

    Note:
        The performance metrics can be used to assess the efficiency, speed, memory consumption,
        and resource utilization of the cryptographic algorithm in a performance evaluation context.
    """
    
        # Start tracemalloc to measure memory usage
    
    
    # Perform input validation
    if not callable(func):
        raise ValueError("The 'func' parameter must be a callable function.")
    
    # Measure implementation size
    implementation_size = sys.getsizeof(func)

    # Measure input size
    input_size = 0
    for arg in args :
        input_size += sys.getsizeof(arg)

    tracemalloc.start()

    # Measure time
    start_time = time.process_time()
    result = func(*args, **kwargs)
    end_time = time.process_time()

    # Stop tracemalloc and get memory usage statistics
    current, peak = tracemalloc.get_traced_memory()
    tracemalloc.stop()

    # Calculate RAM consumption
    ram_consumption = peak - current

    # Calculate execution time in seconds
    execution_time = end_time - start_time

    # Calculate throughput (bytes/second)
    if execution_time == 0 : 
        throughput = 0
    else :
        throughput = input_size / execution_time
    


    return [
        result,
        implementation_size,
        ram_consumption,
        throughput,
        execution_time,
        #current,
        #cpu_cycles,
    ]


def average_measurements(num_iterations, measurement_func, *args, **kwargs):
    """
    Performs multiple measurements of a function's performance and calculates the average metrics.

    Parameters:
        num_iterations (int): The number of measurements to be performed.
        measurement_func (function): The function for measuring performance.
        *args: Variable-length argument list for the measurement function.
        **kwargs: Arbitrary keyword arguments for the measurement function.

    Returns:
        list: A list containing the average metrics over multiple measurements:
              [result, avg_implementation_size, avg_ram_consumption, avg_throughput,
               avg_execution_time, avg_memory_usage, avg_cpu_cycles]
    """
    metrics_sum = [0] * len(measurement_func(*args, **kwargs))  # Initialize the metrics sum as a list of zeros

    for iteration in range(num_iterations):
        metrics = measurement_func(*args, **kwargs)  # Perform a single measurement

        # Accumulate the metrics by summing up individual values
        metrics_sum[0] = metrics[0]
        for j in range(1, len(metrics)):
            metrics_sum[j] += metrics[j]

    # Calculate the average by dividing the sum by the number of iterations
    metrics_sum[0] = metrics[0]
    for j in range(1, len(metrics)):
        metrics_sum[j] /= num_iterations

    return metrics_sum


def pkcs7_padding(plaintext, block_size):
    if len(plaintext) % block_size == 0:
        return plaintext
    else :
        padding_size = block_size - len(plaintext) % block_size
        padding = bytes([padding_size] * padding_size)
        return plaintext + padding


def pkcs7_unpadding(plaintext, block_size):
    padding_size = plaintext[-1]

    # Check padding size
    if padding_size <= 0 or padding_size > block_size:
        return plaintext

    # Check padding bytes
    padding_bytes = plaintext[-padding_size:]
    for byte in padding_bytes:
        if byte != padding_size:
            raise ValueError("Invalid padding bytes")

    return plaintext[:-padding_size]
