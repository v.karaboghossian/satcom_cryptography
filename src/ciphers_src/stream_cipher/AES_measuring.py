import Crypto.Cipher.AES as AES
import os

from src.ciphers_src.util import measure_software_performance

'''
        AES :
            works with 16 (128 bits), 24 (192 bits), or 32-byte (256 bits) long key
            For MODE_CTR (stream mode of AES) nonce length must be in the range 0 to 15 Bytes
        (recommended: 8).
        

'''

MODE = AES.MODE_CTR

#LENGTHS = [[16,nonce] for nonce in range(16)] + [[24 , nonce] for nonce in range(16)] + [[32,nonce] for nonce in range(16)]
LENGTHS = [[16,nonce] for nonce in [8,12]] + [[24 , nonce] for nonce in [8,12]] + [[32,nonce] for nonce in [8,12]]
KEY_SIZES = [length[0] for length in LENGTHS]
NONCE_SIZES = [length[1] for length in LENGTHS]
KEYS = [os.urandom(key_size) for key_size in KEY_SIZES]  # Generate a random N-byte long key
NONCES = [os.urandom(nonce_size) for nonce_size in NONCE_SIZES]



def encryption_measurement(plaintext, key, nonce, mode):
    perf = measure_software_performance(AES.new, key=key, nonce=nonce, mode=mode)
    cipher = perf[0]
    metrics = [object for object in perf[1:]]
    metrics = [b''] + metrics
    perf = measure_software_performance(cipher.encrypt, plaintext)
    metrics = [metrics[i] + perf[i] for i in range(len(perf))]

    return metrics


def decryption_measurement(encryptedtext, key, nonce, mode):
    perf = measure_software_performance(AES.new, key=key, nonce=nonce, mode=mode)
    cipher = perf[0]
    metrics = [object for object in perf[1:]]
    metrics = [b''] + metrics
    perf = measure_software_performance(cipher.decrypt, encryptedtext)
    metrics = [metrics[i] + perf[i] for i in range(len(perf))]

    return metrics

