import Crypto.Cipher.Salsa20 as salsa
import os

from src.ciphers_src.util import measure_software_performance

'''
        SALSA :
            key 256 bits long (32 bytes), but it can also work with 128 bit (16 bytes).
            nonce size is 64 bits (8 bytes)
        
        stream ciphers don't require padding
'''


MODE = ""
LENGTHS = [[32,8],[16,8]]
KEY_SIZES = [length[0] for length in LENGTHS]
NONCE_SIZES = [length[1] for length in LENGTHS]
KEYS = [os.urandom(key_size) for key_size in KEY_SIZES]  # Generate a random N-byte long key
NONCES = [os.urandom(nonce_size) for nonce_size in NONCE_SIZES]



def encryption_measurement(plaintext, key, nonce, mode):
    perf = measure_software_performance(salsa.new, key=key, nonce=nonce)
    cipher = perf[0]
    metrics = [object for object in perf[1:]]

    metrics = [b''] + metrics
    perf = measure_software_performance(cipher.encrypt, plaintext)
    metrics = [metrics[i] + perf[i] for i in range(len(perf))]

    return metrics


def decryption_measurement(encryptedtext, key, nonce, mode):
    perf = measure_software_performance(salsa.new, key=key, nonce=nonce)
    cipher = perf[0]
    metrics = [object for object in perf[1:]]

    metrics = [b''] + metrics
    perf = measure_software_performance(cipher.decrypt, encryptedtext)
    metrics = [metrics[i] + perf[i] for i in range(len(perf))]

    return metrics


