import Crypto.Cipher.ChaCha20 as chacha
import os

from src.ciphers_src.util import measure_software_performance
'''
        CHACHA :
            key is 256 bits long (32 bytes)
            nonce is 8, 12 or 24 bytes
        
        stream ciphers don't require padding
'''

MODE = ""
LENGTHS = [[32,8],[32,12],[32,24]]
KEY_SIZES = [length[0] for length in LENGTHS]
NONCE_SIZES = [length[1] for length in LENGTHS]
KEYS = [os.urandom(key_size) for key_size in KEY_SIZES]
NONCES = [os.urandom(nonce_size) for nonce_size in NONCE_SIZES]


def encryption_measurement(plaintext, key, nonce, mode):
    perf = measure_software_performance(chacha.new, key=key, nonce=nonce)
    cipher = perf[0]
    metrics = [object for object in perf[1:]]

    metrics = [b''] + metrics
    perf = measure_software_performance(cipher.encrypt, plaintext)
    metrics = [metrics[i] + perf[i] for i in range(len(perf))]

    return metrics


def decryption_measurement(encryptedtext, key, nonce, mode):
    perf = measure_software_performance(chacha.new, key=key, nonce=nonce)
    cipher = perf[0]
    metrics = [object for object in perf[1:]]

    metrics = [b''] + metrics
    perf = measure_software_performance(cipher.decrypt, encryptedtext)
    metrics = [metrics[i] + perf[i] for i in range(len(perf))]

    return metrics


