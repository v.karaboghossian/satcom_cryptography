import src.ciphers_src.stream_cipher.AES_measuring as aes_stream
import src.ciphers_src.stream_cipher.CHACHA20_measuring as chacha
import src.ciphers_src.stream_cipher.SALSA20_measuring as salsa
from src.ciphers_src.util import average_measurements as average_measurements_stream
import src.ciphers_src.block_cipher.AES_measuring as aes_block
import src.ciphers_src.block_cipher.DES_measuring as des
import src.ciphers_src.block_cipher.SPECK_measuring as speck
import src.ciphers_src.block_cipher.SIMON_measuring as simon
import src.ciphers_src.block_cipher.PRESENT_measuring as present
from src.ciphers_src.util import average_measurements as average_measurements_block

import sys



CIPHER = {
    'stream' : {
        "AES" : aes_stream,
        "CHACHA" : chacha,
        "SALSA" : salsa,
    },
    'block' :{
    "AES" : aes_block,
    "DES" : des,
    "SIMON" : simon,
    "SPECK" : speck,
    "PRESENT" : present
    }
}


class node :
    def __init__(self,storage_capacity, RAM_capacity, processor_frequency, plaintext) -> None:
        self.storage_capacity = storage_capacity
        self.RAM_capacity = RAM_capacity
        self.processor_frequency = processor_frequency
        stream = self._cipher_analysis('stream', plaintext)
        block = self._cipher_analysis('block', plaintext)
        integrity = (stream[1]==block[1]) == True
        self.ciphers_data = {
            "intergity" : integrity,
            "stream" : stream[0], 
            "block" : block[0]
        }
        self.normalization()


    def _cipher_analysis(self, mode, plaintext):

        ALL_TRUE = True
        NUM_ITERATIONS = 100

        datas = {}

        for cipher in CIPHER[mode].keys():

            datas[cipher] = {}

            MODE = CIPHER[mode][cipher].MODE
            LENGTHS = CIPHER[mode][cipher].LENGTHS
            KEY_SIZES = CIPHER[mode][cipher].KEY_SIZES
            KEYS = CIPHER[mode][cipher].KEYS
            if mode == 'stream' :
                NONCE_BLOCK_SIZES = CIPHER[mode][cipher].NONCE_SIZES
                NONCES = CIPHER[mode][cipher].NONCES
            if mode == 'block' :
                NONCE_BLOCK_SIZES = CIPHER[mode][cipher].BLOCK_SIZES


            for scenario in range(len(LENGTHS)):

                KEY = KEYS[scenario]
                KEY_SIZE = KEY_SIZES[scenario]
                NONCE_BLOCK_SIZE = NONCE_BLOCK_SIZES[scenario]
                
                if mode == 'stream' :
                    NONCE = NONCES[scenario]

                    print(f"{cipher} with key of {KEY_SIZE}byte and nonce of {NONCE_BLOCK_SIZE}byte in process ...")
                    encryption = average_measurements_stream(NUM_ITERATIONS, CIPHER[mode][cipher].encryption_measurement,
                                        plaintext, KEY, NONCE, MODE)
                    decryption = average_measurements_stream(NUM_ITERATIONS, CIPHER[mode][cipher].decryption_measurement,
                                                    encryption[0], KEY, NONCE, MODE)
                

                if mode == 'block' :
                    BLOCK_SIZE = NONCE_BLOCK_SIZES[scenario]

                    print(f"{cipher} with key of {KEY_SIZE}byte and block of {NONCE_BLOCK_SIZE}byte in process ...")
                    encryption = average_measurements_block(NUM_ITERATIONS, \
                                                            CIPHER[mode][cipher].encryption_measurement,\
                                                            plaintext, KEY, KEY_SIZE, BLOCK_SIZE, MODE)
                    decryption = average_measurements_block(NUM_ITERATIONS, \
                                                            CIPHER[mode][cipher].decryption_measurement,\
                                                            encryption[0], KEY, KEY_SIZE, BLOCK_SIZE, MODE)



                if not (decryption[0] == plaintext):
                    ALL_TRUE = False
                
                metrics = {
                    "integrity": decryption[0] == plaintext,
                    "encryption" : {
                        "implementation_size": encryption[1],
                        "ram_consumption": encryption[2],
                        "throughput": encryption[3],
                        "execution_time": encryption[4],
                        #"memory_usage": encryption[5],
                        "encrypted_file_size": sys.getsizeof(encryption[0]),
                        #"cpu_cycles": [encryption[6], decryption[6], encryption[6]+ decryption[6]],
                    },
                    'decryption' : {
                        "implementation_size": decryption[1],
                        "ram_consumption": decryption[2],
                        "throughput": decryption[3],
                        "execution_time": decryption[4],
                        #"memory_usage": decryption[5],
                        "encrypted_file_size": 0,
                        #"cpu_cycles": [encryption[6], decryption[6], encryption[6]+ decryption[6]],
                    },
                    'encryption+decryption' : {
                        "implementation_size": encryption[1]+decryption[1],
                        "ram_consumption": encryption[2]+ decryption[2],
                        "throughput": encryption[3]+ decryption[3],
                        "execution_time": encryption[4]+ decryption[4],
                        #"memory_usage": encryption[5]+ decryption[5],
                        "encrypted_file_size": 0,
                        #"cpu_cycles": [encryption[6], decryption[6], encryption[6]+ decryption[6]],
                    },
                    
                }
                datas[cipher][cipher+'-'+str(KEY_SIZE)+'/'+str(NONCE_BLOCK_SIZE)] = metrics
        
        return datas, ALL_TRUE
    

    def normalization(self):
        
        for mode in ['stream','block']:
            min_max_metrics = { 
                    'encryption' : {
                        "implementation_size": [],
                        "ram_consumption": [],
                        "throughput": [],
                        "execution_time": [],
                        #"memory_usage": [],
                        "encrypted_file_size": [],
                        #"cpu_cycles": [encryption[6], decryption[6], encryption[6]+ decryption[6]],
                    },
                    'decryption' : {
                        "implementation_size": [],
                        "ram_consumption": [],
                        "throughput": [],
                        "execution_time": [],
                        #"memory_usage": [],
                        "encrypted_file_size": [],
                        #"cpu_cycles": [encryption[6], decryption[6], encryption[6]+ decryption[6]],
                    },
                    'encryption+decryption' : {
                        "implementation_size": [],
                        "ram_consumption": [],
                        "throughput": [],
                        "execution_time": [],
                        #"memory_usage": [],
                        "encrypted_file_size": [],
                        #"cpu_cycles": [encryption[6], decryption[6], encryption[6]+ decryption[6]],
                    },
            }

            # add them all in a min_max_metrics dictionnary
            for cipher in self.ciphers_data[mode].keys() :
                for combinaison in self.ciphers_data[mode][cipher].keys():
                    for operation in self.ciphers_data[mode][cipher][combinaison].keys():
                        if operation != "integrity":
                            for metric in self.ciphers_data[mode][cipher][combinaison][operation].keys():
                                min_max_metrics[operation][metric].append(self.ciphers_data[mode][cipher][combinaison][operation][metric])

            # just keep the max and min value
            for operation in min_max_metrics.keys():
                for metric in min_max_metrics[operation].keys():
                    maxim = max(min_max_metrics[operation][metric])
                    minim = min(min_max_metrics[operation][metric])
                    min_max_metrics[operation][metric] = [minim, maxim]

            # now normalize
            for cipher in self.ciphers_data[mode].keys() :
                for combinaison in self.ciphers_data[mode][cipher].keys():
                    for operation in self.ciphers_data[mode][cipher][combinaison].keys():
                        if operation != "integrity":
                            for metric in self.ciphers_data[mode][cipher][combinaison][operation].keys():
                                maxim = min_max_metrics[operation][metric][1]
                                minim = min_max_metrics[operation][metric][0]
                                if [maxim, minim] != [0,0]:
                                    if maxim == minim :
                                        minim=0
                                    if not (metric == "throughput") :
                                        self.ciphers_data[mode][cipher][combinaison][operation][metric] -= minim
                                    else : 
                                        self.ciphers_data[mode][cipher][combinaison][operation][metric] = \
                                            maxim - self.ciphers_data[mode][cipher][combinaison][operation][metric]
                                    self.ciphers_data[mode][cipher][combinaison][operation][metric] /= (maxim-minim)

        
  


class scenario :

    def __init__(self, node1, node2, bandwidth, distance, signal_celerity, plaintext) -> None:
        self.node1 = node1
        self.node2 = node2
        self.bandwidth = bandwidth
        self.distance = distance
        self.latency = distance/signal_celerity

        self.weights = self._weights(plaintext)
        self.costs = {
            'stream' : self._ciphers_cost('stream', plaintext),
            'block' : self._ciphers_cost('block', plaintext)
        }


    def _weights(self, message):

        weights = {}
        
        for node in ["node1", "node2"]:

            weights[node] = {}

            weights[node]["implementation_size"] = 1/(self.node1.storage_capacity)
            weights[node]["memory_usage"] = 1/(self.node1.RAM_capacity)
            
            weights[node]["throughput"] = \
                1/(len(message) * self.node1.processor_frequency)
            
            weights[node]["execution_time"] = \
                1/(self.latency)
            
            weights[node]["encrypted_file_size"] = \
                1/(self.latency * self.bandwidth)
    
        sum = 0
        for node in weights.keys():
            for metrics in weights[node].keys():
                sum += weights[node][metrics]

        for node in weights.keys():
            for metrics in weights[node].keys():
                weights[node][metrics]/=sum

        return weights
    

    def _ciphers_cost(self, mode, message):

        WEIGHTS = self.weights
        costs = {}

        for cipher in self.node1.ciphers_data[mode].keys():
            costs[cipher] = {}
            for ciphers_combination in self.node1.ciphers_data[mode][cipher].keys():

                encryption_data = self.node1.ciphers_data[mode][cipher][ciphers_combination]["encryption"]
                decryption_data = self.node1.ciphers_data[mode][cipher][ciphers_combination]["decryption"]
                cost = 0
                cost += WEIGHTS["node1"]["implementation_size"]*encryption_data["implementation_size"]
                cost += WEIGHTS["node1"]["memory_usage"]*encryption_data["ram_consumption"]
                cost += WEIGHTS["node1"]["throughput"]*encryption_data["throughput"]
                cost += WEIGHTS["node1"]["execution_time"]*encryption_data["execution_time"]
                cost += WEIGHTS["node1"]["encrypted_file_size"]*encryption_data["encrypted_file_size"]

                encryption_data = self.node2.ciphers_data[mode][cipher][ciphers_combination]["encryption"]
                decryption_data = self.node2.ciphers_data[mode][cipher][ciphers_combination]["decryption"]
                cost += WEIGHTS["node2"]["implementation_size"]*decryption_data["implementation_size"]
                cost += WEIGHTS["node2"]["memory_usage"]*decryption_data["ram_consumption"]
                cost += WEIGHTS["node2"]["throughput"]*decryption_data["throughput"]
                cost += WEIGHTS["node2"]["execution_time"]*decryption_data["execution_time"]
                cost += WEIGHTS["node2"]["encrypted_file_size"]*decryption_data["encrypted_file_size"]
                
                costs[cipher][ciphers_combination] = cost
        return costs
    


    def choose_the_better(self):
        updated_costs = {}  # Create a new dictionary to store updated costs

        for mode in self.costs.keys():
            updated_costs[mode] = {}
            for cipher in self.costs[mode].keys():
                updated_costs[mode][cipher] = {}
                best_length_combination = float("inf")
                for combinaison in self.costs[mode][cipher].keys():
                    if self.costs[mode][cipher][combinaison] < best_length_combination:
                        best_length_combination = self.costs[mode][cipher][combinaison]
                for combinaison in self.costs[mode][cipher].keys():
                    if self.costs[mode][cipher][combinaison] <= best_length_combination:
                        # Store the combination in the new dictionary
                        updated_costs[mode][cipher][combinaison] = self.costs[mode][cipher][combinaison]

        # Replace the original costs dictionary with the updated one
        self.costs = updated_costs





